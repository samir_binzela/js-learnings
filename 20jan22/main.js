var httpRequest = new XMLHttpRequest();

httpRequest.open("get", "https://jsonplaceholder.typicode.com/users");

httpRequest.send();

httpRequest.onreadystatechange = function(){
    console.log("state changed",httpRequest.readyState);
    console.log("status",httpRequest.status);
    if(httpRequest.readytState == 4 && httpRequest.status == 200){
      var data =  JSON.parse(httpRequest.responseText);
      console.log(data);
    createTableHead(data[1]);
    createTableBody(data);
    }
}

function createTableBody(users){
    var tbody = [];
    var tr = [];
    var td = [];
    var tobdyRef = document.getElementById("tablebody");
    // console.log("data", data);
    for(var i=0;i<users.length;i++){
        var user = users[i];
        var userval = Object.values(users);
        tr = tr +"<tr>";
        let td = "";

        for(j=0;j<userval.length;j++){
          td+="<td>"+userval[j]+"</td>";
        }
        tr = tr+"</tr>";

        var tbody = tbody + tr + td
        // console.log(user);
        var id = user.id;
        var name = user.name;
        var username = user.username;
        var email = user.email;
        var phone = user.phone;
      tbody = tbody + "<tr>" + "<td>"+id+"</td>" + "<td>"+name+"</td>" + "<td>"+username+"</td>" + "<td>"+email+"</td>" + "<td>"+phone+"</td>" + "</tr>";
    }
    // console.log(tbody);
    tobdyRef.innerHTML=tbody;
}

function createTableHead(object){

    let headers = Object.keys(object);
    var tableHead = [];
    for(i=0;i<headers.length;i++){
      tableHead = tableHead + "<th>"+headers[i]+"</th>";
    }
    // console.log("headers",headers);
    // let tableHead = "<th>"+headers[0]+"<th/>"+"<th>"+headers[1]+"<th/>"+"<th>"+headers[2]+"<th/>"+"<th>"+headers[3]+"<th/>"+"<th>"+headers[5]+"<th/>"
    document.getElementById("tableHead").innerHTML=tableHead;
}