var date = new  Date();

let hours = date.getHours();
let minutes = date.getMinutes();

var timeFormat = "";

if(hours - 12<0){
    timeFormat = "AM";
}
else
{
    timeFormat = "PM";
}
document.getElementById("displayTime").innerHTML = hours + ":" + minutes + "&nbsp;&nbsp"+timeFormat;